# Имя проекта
#-------------------------------------------------------------------------------
TARGET  = template

# Используемые модули библиотеки периферии
#-------------------------------------------------------------------------------
# PERIPHDRIVERS += stm32f10x_adc
# PERIPHDRIVERS += stm32f10x_bkp
# PERIPHDRIVERS += stm32f10x_can
# PERIPHDRIVERS += stm32f10x_cec
# PERIPHDRIVERS += stm32f10x_crc
# PERIPHDRIVERS += stm32f10x_dbgmcu
PERIPHDRIVERS += stm32f10x_dma
# PERIPHDRIVERS += stm32f10x_exti
# PERIPHDRIVERS += stm32f10x_flash
# PERIPHDRIVERS += stm32f10x_fsmc
PERIPHDRIVERS += stm32f10x_gpio
# PERIPHDRIVERS += stm32f10x_i2c
# PERIPHDRIVERS += stm32f10x_iwdg
# PERIPHDRIVERS += stm32f10x_pwr
PERIPHDRIVERS += stm32f10x_rcc
# PERIPHDRIVERS += stm32f10x_rtc
# PERIPHDRIVERS += stm32f10x_sdio
# PERIPHDRIVERS += stm32f10x_spi
PERIPHDRIVERS += stm32f10x_tim
# PERIPHDRIVERS += stm32f10x_usart
# PERIPHDRIVERS += stm32f10x_wwdg
# PERIPHDRIVERS += misc.c

# Дефайны
#-------------------------------------------------------------------------------
# Выберите соответствующее устройство из списка, опции которого скопированы из
# файла stm32f10x.h
#
# STM32F10X_LD 		STM32F10X_LD: STM32 Low density devices
# STM32F10X_LD_VL	STM32F10X_LD_VL: STM32 Low density Value Line devices
# STM32F10X_MD		STM32F10X_MD: STM32 Medium density devices
# STM32F10X_MD_VL	STM32F10X_MD_VL: STM32 Medium density Value Line devices
# STM32F10X_HD		STM32F10X_HD: STM32 High density devices
# STM32F10X_HD_VL	STM32F10X_HD_VL: STM32 High density value line devices
# STM32F10X_XL		STM32F10X_XL: STM32 XL-density devices
# STM32F10X_CL		STM32F10X_CL: STM32 Connectivity line devices
#
# - Low-density devices are STM32F101xx, STM32F102xx and STM32F103xx microcontrollers
#   where the Flash memory density ranges between 16 and 32 Kbytes.
#
# - Low-density value line devices are STM32F100xx microcontrollers where the Flash
#   memory density ranges between 16 and 32 Kbytes.
#
# - Medium-density devices are STM32F101xx, STM32F102xx and STM32F103xx microcontrollers
#   where the Flash memory density ranges between 64 and 128 Kbytes.
#
# - Medium-density value line devices are STM32F100xx microcontrollers where the
#   Flash memory density ranges between 64 and 128 Kbytes.
#
# - High-density devices are STM32F101xx and STM32F103xx microcontrollers where
#   the Flash memory density ranges between 256 and 512 Kbytes.
#
# - High-density value line devices are STM32F100xx microcontrollers where the
#   Flash memory density ranges between 256 and 512 Kbytes.
#
# - XL-density devices are STM32F101xx and STM32F103xx microcontrollers where
#   the Flash memory density ranges between 512 and 1024 Kbytes.
#
# - Connectivity line devices are STM32F105xx and STM32F107xx microcontrollers.
#
# HSE_VALUE sets the value of the HSE clock, 8MHz in this case

DEFINES += STM32F10X_MD_VL  # SystemCoreClock = SYSCLK_FREQ_24MHz
DEFINES += HSE_VALUE=8000000  # устанавливаем внешний кварц в 8МГц
DEFINES += USE_STDPERIPH_DRIVER
DEFINES += GCC_ARMCM3
DEFINES += VECT_TAB_FLASH
# DEFINES += USE_GAMMA_CORRECTION
# DEFINES += USE_PRECALCULATED_GAMMA_TABLE

# Путь к файлу инициализации
#-------------------------------------------------------------------------------
# STARTUP = startup/startup_stm32f10x_cl.s
# STARTUP = startup/startup_stm32f10x_hd.s
# STARTUP = startup/startup_stm32f10x_hd_vl.s
# STARTUP = startup/startup_stm32f10x_ld.s
# STARTUP = startup/startup_stm32f10x_ld_vl.s
STARTUP = startup/startup_stm32f10x_md_vl.s
# STARTUP = startup/startup_stm32f10x_md_vl.s
# STARTUP = startup/startup_stm32f10x_xl.s

# Инструменты
#-------------------------------------------------------------------------------
TOOLCHAIN=~/devel/arm/toolchain/current/bin
AS = $(TOOLCHAIN)/arm-none-eabi-gcc
CC = ccache $(TOOLCHAIN)/arm-none-eabi-gcc
LD = $(TOOLCHAIN)/arm-none-eabi-gcc
CP = $(TOOLCHAIN)/arm-none-eabi-objcopy
SZ = $(TOOLCHAIN)/arm-none-eabi-size
OD = $(TOOLCHAIN)/arm-none-eabi-objdump
RM = rm
TEST_AS = gcc
TEST_CC = gcc
TEST_LD = gcc

# Пути к CMSIS, StdPeriph Lib
#-------------------------------------------------------------------------------
CMSIS_PATH         = cmsis
STDPERIPH_INC_PATH = stdperiph/inc
STDPERIPH_SRC_PATH = stdperiph/src
WS2812B_INC_PATH = ws2812b/inc
WS2812B_SRC_PATH = ws2812b/src

# Пути поиска исходных файлов
#-------------------------------------------------------------------------------
SOURCEDIRS := src
SOURCEDIRS += $(CMSIS_PATH)
SOURCEDIRS += $(STDPERIPH_SRC_PATH)
SOURCEDIRS += $(WS2812B_SRC_PATH)

# Пути поиска заголовочных файлов
#-------------------------------------------------------------------------------
INCLUDES += .
INCLUDES += $(SOURCEDIRS)
INCLUDES += $(CMSIS_PATH)
INCLUDES += $(STDPERIPH_INC_PATH)
INCLUDES += $(WS2812B_INC_PATH)

# Библиотеки
#-------------------------------------------------------------------------------
LIBPATH += $(WS2812B_SRC_PATH)
LIBS    +=

# Настройки компилятора
#-------------------------------------------------------------------------------
CFLAGS += -mthumb -mcpu=cortex-m3 # архитектура и система комманд
CFLAGS += -std=gnu99              # стандарт языка С
CFLAGS += -Wall -pedantic         # выводить все предупреждения
CFLAGS += -O0                     # оптимизация
CFLAGS += -ggdb                   # генерировать отладочную информацию для gdb
CFLAGS += -fno-builtin

CFLAGS += $(addprefix -I, $(INCLUDES))
CFLAGS += $(addprefix -D, $(DEFINES))

# Скрипт линкера
#-------------------------------------------------------------------------------
LDSCR_PATH = ld-scripts
LDSCRIPT   = stm32f100rb.ld
# LDSCRIPT   = stm32f103c8t6.ld

# Настройки линкера
#-------------------------------------------------------------------------------
LDFLAGS += -nostartfiles -nostdlib -mthumb
LDFLAGS += -L$(LDSCR_PATH)
LDFLAGS += -T$(LDSCR_PATH)/$(LDSCRIPT)
LDFLAGS += $(addprefix -L, $(LIBPATH))
LDFLAGS += $(LIBS)

# Настройки ассемблера
#-------------------------------------------------------------------------------
# AFLAGS += -ahls -mapcs-32

# Список объектных файлов
#-------------------------------------------------------------------------------
OBJS += $(patsubst %.c, %.o, $(wildcard  $(addsuffix /*.c, $(SOURCEDIRS))))
OBJS += $(addprefix $(STDPERIPH_SRC_PATH)/, $(addsuffix .o, $(PERIPHDRIVERS)))
OBJS += $(patsubst %.s, %.o, $(STARTUP))

# Пути поиска make
#-------------------------------------------------------------------------------
VPATH := $(SOURCEDIRS)

# Список файлов к удалению командой "make clean"
#-------------------------------------------------------------------------------

TOREMOVE += *.elf *.hex a.out *~
TOREMOVE += $(addsuffix /*.o, $(SOURCEDIRS))
TOREMOVE += $(addsuffix /*.d, $(SOURCEDIRS))
TOREMOVE += $(addsuffix /*~, $(SOURCEDIRS))
TOREMOVE += $(STDPERIPH_SRC_PATH)/*.o
TOREMOVE += $(patsubst %.s, %.o, $(STARTUP))
TOREMOVE += $(TARGET)


# Собрать всё
#-------------------------------------------------------------------------------
all: $(TARGET).hex size

# Очистка
#-------------------------------------------------------------------------------
clean:
	@$(RM) -f $(TOREMOVE)

# Создание .hex файла
#-------------------------------------------------------------------------------
$(TARGET).hex: $(TARGET).elf $(TARGET).lst
	@echo "Create HEX =>" $(CP) -Oihex $(TARGET).elf $(TARGET).hex
	@$(CP) -Oihex $(TARGET).elf $(TARGET).hex

$(TARGET).lst: $(TARGET).elf
	@echo "Create LST =>" $(OD) -h -S $(TARGET).elf > $(TARGET).lst
	@$(OD) -h -S $< > $@

# Показываем размер
#-------------------------------------------------------------------------------
size:
	@echo "---------------------------------------------------"
	@$(SZ) $(TARGET).elf

# Линковка
#-------------------------------------------------------------------------------
$(TARGET).elf: $(OBJS)
	@echo "Linking =>" $^
	@$(LD) $(LDFLAGS) $^ -o $@

# Компиляция
#-------------------------------------------------------------------------------
%.o: %.c
	@echo "Compilling C source =>" $<
	@$(CC) $(CFLAGS) -MD -c $< -o $@

%.o: %.s
	@echo "Compilling ASM source =>" $<
	@$(AS) $(AFLAGS) -c $< -o $@

# Сгенерированные gcc зависимости
#-------------------------------------------------------------------------------
include $(wildcart *.d)


# Тестирование
#-------------------------------------------------------------------------------
UNITY_PATH = unity

TESTS_DEFINES += USE_STDPERIPH_DRIVER STM32F10X_MD_VL UNITTEST
# TESTS_DEFINES += TEST_PRINTS
# TESTS_DEFINES += USE_GAMMA_CORRECTION
# TESTS_DEFINES += USE_PRECALCULATED_GAMMA_TABLE
TESTS_SOURCEDIR := tests $(WS2812B_SRC_PATH)
TESTS_INCLUDES := tests $(CMSIS_PATH) $(STDPERIPH_INC_PATH) $(WS2812B_INC_PATH) $(UNITY_PATH)
TESTS_LIBPATH := $(WS2812B_SRC_PATH) $(UNITY_PATH)

.PHONY: tests
tests: AS = $(TEST_AS)
tests: CC = $(TEST_CC)
tests: LD = $(TEST_LD)
tests: CFLAGS = -std=gnu99 -O0 $(addprefix -D, $(TESTS_DEFINES)) $(addprefix -I, $(TESTS_INCLUDES))
tests: LDFLAGS = $(addprefix -L, $(TESTS_LIBPATH))
tests: OBJS = tests/test.o ws2812b/src/bitmap.o ws2812b/src/ws2812b.o $(UNITY_PATH)/unity.o

tests:
	@echo "AS =" $(AS) "; CC =" $(CC) "; LD =" $(LD)
	@echo "CFLAGS =" $(CFLAGS)
	@echo "LDFLAGS =" $(LDFLAGS)
	@echo "OBJS =" $(OBJS)

	@$(CC) $(CFLAGS) -MD -c $(UNITY_PATH)/unity.c -o $(UNITY_PATH)/unity.o
	@$(CC) $(CFLAGS) -MD -c tests/test.c -o tests/test.o
	@$(CC) $(CFLAGS) -MD -c ws2812b/src/bitmap.c -o ws2812b/src/bitmap.o
	@$(CC) $(CFLAGS) -MD -c ws2812b/src/ws2812b.c -o ws2812b/src/ws2812b.o

	@echo "Linking =>" $(OBJS)
	@$(LD) $(LDFLAGS) $(OBJS) -o $@.run
