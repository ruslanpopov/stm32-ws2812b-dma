#include <stm32f10x.h>
/* Подключаем функции управления генератором частоты и GPIO */
#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#include "ws2812b.h"

#define NUM_LEDS    256
#define COLOR_VALUE 10

RGB_t leds[NUM_LEDS];

const uint16_t
     LED1 = GPIO_Pin_8, // PC8
     LED2 = GPIO_Pin_9, // PC9
     BUTTON = GPIO_Pin_0; // PA0

typedef enum {FALSE, TRUE} BOOLEAN_T;

BOOLEAN_T r = TRUE;
BOOLEAN_T g = FALSE;
BOOLEAN_T b = FALSE;


void init_button();
void init_leds();


int main()
{
     SystemInit();

     init_button();
     init_leds();

     /* Конфигурируем таймер SysTick на срабатывание 100 раз в секунду */
     SysTick_Config(SystemCoreClock / 100);

     ws2812b_init();

     while (1)
     {
          while (!ws2812b_is_ready()); // wait

          // Заполнить RGB-буффер
          uint16_t i;
          for (i=0; i<NUM_LEDS; i++)
          {
               uint8_t value = i % 64;
               RGB_t led = {
                    r ? value : 0,
                    g ? value : 0,
                    b ? value : 0
               };
               leds[i] = led;
          }

          ws2812b_send_rgb(leds, NUM_LEDS);
     }
}


/**
 * Обработчик прерывания по переполнению таймера SysTick
 */
void SysTick_Handler()
{
     /* Обработка кнопки */
     static uint8_t btn_old_state = 0;
     uint8_t btn_state = GPIO_ReadInputDataBit(GPIOA, BUTTON);

     if (btn_old_state == 0 && btn_state == 1)
     {
          GPIO_WriteBit(GPIOC, LED1, !GPIO_ReadOutputDataBit(GPIOC, LED1));

          if (r) {
               r = FALSE; g = TRUE; b = FALSE;
          } else if (g) {
               r = FALSE; g = FALSE; b = TRUE;
          } else if (b > 0) {
               r = TRUE; g = FALSE; b = FALSE;
          }
     }
     btn_old_state = btn_state;

     /* Мигание светодиодом */
     static uint8_t counter = 0;

     if (counter == 0)
     {
          GPIO_WriteBit(GPIOC, LED2, !GPIO_ReadOutputDataBit(GPIOC, LED2));
          counter = 10;
     }
     else
          --counter;
}

/**
 * Инициализация кнопок на плате.
 */
void init_button()
{
     /* Включаем тактирование порта A */
     RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
     /* Заполняем структуру gpio данными для инициализации:
      * - Режим: ввод, Push-Pull
      * - Пины: 0
      * - Частота опроса: 2 МГц
      */
     GPIO_InitTypeDef gpio;
     GPIO_StructInit(&gpio);
     gpio.GPIO_Mode = GPIO_Mode_IN_FLOATING;
     gpio.GPIO_Pin = BUTTON;
     gpio.GPIO_Speed = GPIO_Speed_2MHz;
     GPIO_Init(GPIOA, &gpio);
}


/**
 * Инициализация светодиодов на плате.
 */
void init_leds()
{
     /* Включаем тактирование порта C */
     RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

     /* Заполняем структуру gpio данными для инициализации:
      * - Режим: вывод, Push-Pull
      * - Пины: 8 и 9
      * - Частота обновления: 2 МГц
      */
     GPIO_InitTypeDef gpio;
     GPIO_StructInit(&gpio);
     gpio.GPIO_Mode = GPIO_Mode_Out_PP;
     gpio.GPIO_Pin = LED1 | LED2;
     gpio.GPIO_Speed = GPIO_Speed_2MHz;

     /* Инициализируем GPIO на порту C */
     GPIO_Init(GPIOC, &gpio);

     /* Устанавливаем нули на выводах 8 и 9 */
     GPIO_ResetBits(GPIOC, LED1 | LED2);

}
