/**
 * Тестовый модуль для проверки преобразования из RGB в PWM.
 */

#include <stdio.h>

#include <stm32f10x_tim.h>
#include <stm32f10x_dma.h>

#include "unity.h"
#include "ws2812b.h"


void DMA_SetCurrDataCounter(DMA_Channel_TypeDef* DMAy_Channelx, uint16_t DataNumber)
{
}

ITStatus DMA_GetITStatus(uint32_t DMAy_IT)
{
    return RESET;
}

void DMA_ClearITPendingBit(uint32_t DMAy_IT)
{
}

void DMA_Cmd(DMA_Channel_TypeDef* DMAy_Channelx, FunctionalState NewState)
{
}

void TIM_Cmd(TIM_TypeDef* TIMx, FunctionalState NewState)
{
}


#define NUM_LEDS 8

RGB_t leds[NUM_LEDS];
PWM_t buffer[NUM_LEDS];


void print_as_binary(uint8_t value)
{
    uint8_t mask = 0x80;
    printf("%d\t", value);
    while(mask)
    {
        printf(value & mask ? "1" : "0");
        mask >>= 1;
    }
    printf("\n");
}


void print_rgb(RGB_t value)
{
    printf("R=");
    print_as_binary(value.r);
    printf("G=");
    print_as_binary(value.g);
    printf("B=");
    print_as_binary(value.b);
}

void print_pwm(PWM_t value)
{
    int i;
    printf("R=");
    for (i=0; i<BITS; i++)
    {
        printf("%d ", value.r[i]);
    }
    printf("\t");
    printf("G=");
    for (i=0; i<BITS; i++)
    {
        printf("%d ", value.g[i]);
    }
    printf("\t");
    printf("B=");
    for (i=0; i<BITS; i++)
    {
        printf("%d ", value.b[i]);
    }
    printf("\n");
}

void rgb_to_pwm(PWM_t *pwm, RGB_t value)
{
     uint8_t mask = 0x80;
     for (uint8_t i=0; i<8; i++)
     {
          pwm->r[i] = value.r & mask ? PULSE_HIGH : PULSE_LOW;
          pwm->g[i] = value.g & mask ? PULSE_HIGH : PULSE_LOW;
          pwm->b[i] = value.b & mask ? PULSE_HIGH : PULSE_LOW;
          mask >>= 1;
     }
}


void fill_rgb_buffer(void)
{
    uint16_t i;
    for (i=0; i<NUM_LEDS; i++)
    {
        RGB_t led = {100, i*10, 200};
        leds[i] = led;
    }
}


void test_rgb2pwm(void)
{
    fill_rgb_buffer();

    RGB_t act = leds[0];
    RGB_t exp = {100, 0, 200};
    TEST_ASSERT_EQUAL_MEMORY(&exp, &act, sizeof(RGB_t));

    int i;

    RGB_t *src = leds;
    PWM_t *dst = buffer;

    for (i=0; i<NUM_LEDS; i++)
    {
        rgb2pwm(src+i, dst+i);
#ifdef TEST_PRINTS
        print_rgb(leds[i]);
        print_pwm(buffer[i]);
#endif
        PWM_t exp;
        rgb_to_pwm(&exp, src[i]);
        TEST_ASSERT_EQUAL_MEMORY(&exp, &dst[i], sizeof(PWM_t));
    }
}


void test_null_buffer(void)
{
    PWM_t *pwm = buffer;

    null_buffer(&pwm, NUM_LEDS);
#ifdef TEST_PRINTS
    print_pwm(buffer[0]);
    print_pwm(buffer[NUM_LEDS-1]);
#endif
}


int main()
{
    UNITY_BEGIN();
    RUN_TEST(test_null_buffer);
    RUN_TEST(test_rgb2pwm);
    return UNITY_END();
}
