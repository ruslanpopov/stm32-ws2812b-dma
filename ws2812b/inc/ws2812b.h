// The MIT License (MIT)
//
// Copyright (c) 2015 Aleksandr Aleshin <silencer@quadrius.net>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef __WS2812B_H
#define __WS2812B_H

#ifdef UNITTEST
#include <stdio.h>
#include <stm32f10x.h>
#endif

#include <stdint.h>
#include "bitmap.h"

#define BITS            8  // количество бит в байте
#define START_SIZE      2
#define BUFFER_SIZE     64
#define HALF_SIZE       (BUFFER_SIZE / 2)
#define PERIOD          30  // 800 кГц
#define PULSE_HIGH      21
#define PULSE_LOW       9

/**
 * TIM4_CH2 привязан к DMA1_CH4
 */
#define WS2812B_PIN     GPIO_Pin_7
#define WS2812B_DMA     DMA1_Channel4
#define WS2812B_DMA_IRQ DMA1_Channel4_IRQn
#define WS2812B_DMA_HND DMA1_Channel4_IRQHandler
#define WS2812B_DMA_HT  DMA1_IT_HT4
#define WS2812B_DMA_TC  DMA1_IT_TC4
#define WS2812B_TIM     TIM4
#define WS2812B_TIM_CC  TIM_DMA_CC2
#define WS2812B_TIM_CCR (WS2812B_TIM->CCR2)
#define WS2812B_TIM_OCI TIM_OC2Init
#define WS2812B_TIM_OCP TIM_OC2PreloadConfig


#if defined(__ICCxARM__)
__packed struct PWM
#else
struct __attribute__((packed)) PWM
#endif
{
     // обратите внимание на порядок полей: GRB!
     uint16_t g[BITS], r[BITS], b[BITS];
};

typedef struct PWM PWM_t;
typedef void (FILTER_t)(void **, PWM_t **, uint16_t *, uint16_t);


#ifdef UNITTEST
void null_buffer(PWM_t **pwm, const uint16_t size);
void rgb2pwm(RGB_t *rgb, PWM_t *pwm);
#endif


void ws2812b_init(void);

int ws2812b_is_ready(void);

void ws2812b_send_rgb(RGB_t *rgb, const uint16_t count);
void ws2812b_send_hsv(HSV_t *hsv, const uint16_t count);

#endif //__WS2812B_H
