// The MIT License (MIT)
//
// Copyright (c) 2015 Aleksandr Aleshin <silencer@quadrius.net>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <stdint.h>
#include <string.h>

#include <stm32f10x.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_tim.h>
#include <stm32f10x_dma.h>
#include <misc.h>

#include "ws2812b.h"

//------------------------------------------------------------
// Internal
//------------------------------------------------------------

#define MIN(a, b)   ({ typeof(a) a1 = a; typeof(b) b1 = b; a1 < b1 ? a1 : b1; })


#ifdef USE_GAMMA_CORRECTION
#ifdef USE_PRECALCULATED_GAMMA_TABLE
static const uint8_t LED_GAMMA_TABLE[] = {
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2,
     2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 7, 7, 7, 8, 8, 8, 9, 9, 9,
     10, 10, 11, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15, 16, 16, 17, 17, 18, 18,
     19, 19, 20, 20, 21, 21, 22, 23, 23, 24, 24, 25, 26, 26, 27, 28, 28, 29, 30,
     30, 31, 32, 32, 33, 34, 35, 35, 36, 37, 38, 38, 39, 40, 41, 42, 42, 43, 44,
     45, 46, 47, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 56, 57, 58, 59, 60, 61,
     62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 73, 74, 75, 76, 77, 78, 79, 80, 81,
     82, 84, 85, 86, 87, 88, 89, 91, 92, 93, 94, 95, 97, 98, 99, 100, 102, 103,
     104, 105, 107, 108, 109, 111, 112, 113, 115, 116, 117, 119, 120, 121, 123,
     124, 126, 127, 128, 130, 131, 133, 134, 136, 137, 139, 140, 142, 143, 145,
     146, 148, 149, 151, 152, 154, 155, 157, 158, 160, 162, 163, 165, 166, 168,
     170, 171, 173, 175, 176, 178, 180, 181, 183, 185, 186, 188, 190, 192, 193,
     195, 197, 199, 200, 202, 204, 206, 207, 209, 211, 213, 215, 217, 218, 220,
     222, 224, 226, 228, 230, 232, 233, 235, 237, 239, 241, 243, 245, 247, 249,
     251, 253, 255
};
#endif
#endif

static inline uint8_t correct_gamma(uint8_t v)
{
#ifdef USE_GAMMA_CORRECTION
#ifdef USE_PRECALCULATED_GAMMA_TABLE
     return LED_GAMMA_TABLE[v];
#else
     return (v * v + v) >> 8;
#endif
#else
     return v;
#endif
}

static volatile int dma_busy;

static PWM_t dma_buffer[BUFFER_SIZE];

static FILTER_t *dma_filter;
static void *dma_source;
static uint16_t dma_count;


/**
 * Преобразование RGB в набор величин для реализации ШИМ для каждого цифрового
 * канала. Преобразуется только одно значение!
 */
#ifndef UNITTEST
static
#endif
void rgb2pwm(RGB_t *rgb, PWM_t *pwm)
{
     // коррекция гаммы
     uint8_t r = correct_gamma(rgb->r);
     uint8_t g = correct_gamma(rgb->g);
     uint8_t b = correct_gamma(rgb->b);

     // начинаем со старшего бита
     uint8_t mask = 0x80;

     for (int i = 0; i < BITS; i++)
     {
          pwm->r[i] = r & mask ? PULSE_HIGH : PULSE_LOW;
          pwm->g[i] = g & mask ? PULSE_HIGH : PULSE_LOW;
          pwm->b[i] = b & mask ? PULSE_HIGH : PULSE_LOW;

          mask >>= 1; // переходим к следующему биту
     }
}


/**
 * Обеспечивает генерацию импульса для применения данных из предыдущего пакета.
 * Указатель в буфере ШИМ данных смещается на величину стартового блока пакета.
 */
#ifndef UNITTEST
static
#endif
void null_buffer(PWM_t **pwm, const uint16_t size)
{
     memset(*pwm, 0, size * sizeof(PWM_t));
     *pwm += size;
}


/**
 * Обеспечиваем заполнение ШИМ буфера данными из исходного буфера, преобразуя
 * их из формата RGB.
 */
static void rgb_filter(void **src, PWM_t **pwm, uint16_t *count, uint16_t size)
{
     RGB_t *rgb = (RGB_t *) *src;
     PWM_t *p = *pwm;

     *count -= size; // уменьшаем значение счётчика передаваемых данных

     while (size--)
     {
          rgb2pwm(rgb++, p++);
     }

     *src = rgb;
     *pwm = p;
}


/**
 * Обеспечиваем заполнение ШИМ буфера данными из исходного буфера, преобразуя
 * их из формата HSV.
 */
static void hsv_filter(void **src, PWM_t **pwm, uint16_t *count, uint16_t size)
{
     HSV_t *hsv = (HSV_t *) *src;
     PWM_t *p = *pwm;

     *count -= size;

     while (size--)
     {
          RGB_t rgb;

          hsv2rgb(hsv++, &rgb);
          rgb2pwm(&rgb, p++);
     }

     *src = hsv;
     *pwm = p;
}


/**
 * Обеспечивает передачу данных через DMA, используя их для генерации ШИМ.
 *  - filter - функция преобразования, может быть rgb_filter или hsv_filter.
 */
static void dma_send(FILTER_t *filter, void *src, uint16_t count)
{
     if (!dma_busy)
     {
          dma_busy = 1;

          dma_filter = filter;
          dma_source = src;
          dma_count = count;

          // указатели на начало и конец буфера ШИМ данных
          PWM_t *head = dma_buffer;
          PWM_t *tail = &dma_buffer[BUFFER_SIZE];

          // Подготавливаем буфер с данными для ШИМ
          // - пауза для применения предыдущего пакета данных
          null_buffer(&head, START_SIZE);
          // - данные для отображения, в качестве size передаём минимум
          dma_filter(&dma_source, &head, &dma_count, MIN(dma_count, tail-head));
          // - обнуляем остаток буфера
          if (head < tail)
               null_buffer(&head, tail-head);

          // запускаем передачу данных
          DMA_SetCurrDataCounter(WS2812B_DMA, sizeof(dma_buffer) / sizeof(uint16_t));
          TIM_Cmd(WS2812B_TIM, ENABLE);
          DMA_Cmd(WS2812B_DMA, ENABLE);
     }
}


static void dma_send_next(PWM_t *head, PWM_t* tail)
{
     if (!dma_filter)
     {
          // прекращаем передачу
          TIM_Cmd(WS2812B_TIM, DISABLE);
          DMA_Cmd(WS2812B_DMA, DISABLE);

          dma_busy = 0;
     }
     else if (!dma_count)
     {
          // передаваемые данные закончились
          // обнуляем текущую половину буфера
          null_buffer(&head, tail - head);
          // сбрасываем фильтр, что при окончании обработки остановит DMA
          dma_filter = NULL;
     }
     else
     {
          // RGB PWM data
          dma_filter(&dma_source, &head, &dma_count, MIN(dma_count, tail - head));

          // чистим остаток буфера
          if (head < tail)
               null_buffer(&head, tail - head);
     }
}


/**
 * Обработчик прерывания от DMA.
 */
void WS2812B_DMA_HND(void) {
  // обрабатываем первую половину
     if (DMA_GetITStatus(WS2812B_DMA_HT) != RESET)
     {
          DMA_ClearITPendingBit(WS2812B_DMA_HT);
          dma_send_next(dma_buffer, &dma_buffer[BUFFER_SIZE / 2]);
     }

     // обрабатываем вторую половину
     if (DMA_GetITStatus(WS2812B_DMA_TC) != RESET)
     {
          DMA_ClearITPendingBit(WS2812B_DMA_TC);
          dma_send_next(&dma_buffer[BUFFER_SIZE / 2], &dma_buffer[BUFFER_SIZE]);
     }
}


#ifndef UNITTEST
/**
 * Структуры для настройки периферии.
 */

static GPIO_InitTypeDef pwm_pin = {
     .GPIO_Pin = WS2812B_PIN,
     .GPIO_Speed = GPIO_Speed_50MHz,
     .GPIO_Mode = GPIO_Mode_AF_PP
};

// таймер работает на системной частоте, делитель 0, отсчёт идёт
// вверх, перезагрузка таймера осуществляется каждые 1.25мкс, т.е.
// каждые PERIOD тиков таймера.
static TIM_TimeBaseInitTypeDef tim = {
     .TIM_Prescaler = 0,
     .TIM_CounterMode = TIM_CounterMode_Up,
     .TIM_Period = PERIOD - 1,
     .TIM_ClockDivision = TIM_CKD_DIV1
};

// разрешаем работу выхода таймера в режиме PWM1, по умолчанию ШИМ нулевой.
static TIM_OCInitTypeDef pwm_out = {
     .TIM_OCMode = TIM_OCMode_PWM1,
     .TIM_OutputState = TIM_OutputState_Enable,
     .TIM_Pulse = 0,
     .TIM_OCPolarity = TIM_OCPolarity_High
};


static DMA_InitTypeDef dma = {
     .DMA_PeripheralBaseAddr = (uint32_t) &WS2812B_TIM_CCR,
     .DMA_MemoryBaseAddr = (uint32_t) dma_buffer,
     .DMA_DIR = DMA_DIR_PeripheralDST,
     .DMA_BufferSize = sizeof(dma_buffer) / sizeof(uint16_t),
     .DMA_PeripheralInc = DMA_PeripheralInc_Disable,
     .DMA_MemoryInc = DMA_MemoryInc_Enable,
     .DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord,
     .DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord,
     .DMA_Mode = DMA_Mode_Circular,
     .DMA_Priority = DMA_Priority_High,
     .DMA_M2M = DMA_M2M_Disable
};


static NVIC_InitTypeDef nvic = {
     .NVIC_IRQChannel = WS2812B_DMA_IRQ,
     .NVIC_IRQChannelPreemptionPriority = 0,
     .NVIC_IRQChannelSubPriority = 0,
     .NVIC_IRQChannelCmd = ENABLE
};
#endif


/**
 * Интерфейс библиотеки
 */

void ws2812b_init(void)
{
#ifndef UNITTEST

     // Включаем тактирование периферии
     RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
     RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
     RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

     // Инициализвция ввода-вывода
     GPIO_Init(GPIOB, &pwm_pin);

     // Инициализация таймера
     TIM_TimeBaseInit(WS2812B_TIM, &tim);

     // Инициализация ШИМ
     WS2812B_TIM_OCI(WS2812B_TIM, &pwm_out); // активируем выход таймера
     WS2812B_TIM_OCP(WS2812B_TIM, TIM_OCPreload_Enable); // перезарядка

     // Инициализация DMA
     DMA_Init(WS2812B_DMA, &dma);

     // Включаем запросы от таймера к DMA
     TIM_DMACmd(WS2812B_TIM, WS2812B_TIM_CC, ENABLE);

     // Инициализация прерывания DMA
     NVIC_Init(&nvic);

     // Разрешаем прерывания от DMA
     DMA_ITConfig(WS2812B_DMA, DMA_IT_HT | DMA_IT_TC, ENABLE);
#endif
}


inline int ws2812b_is_ready(void)
{
     return !dma_busy;
}


void ws2812b_send_rgb(RGB_t *rgb, const uint16_t count)
{
     dma_send(&rgb_filter, rgb, count);
}


void ws2812b_send_hsv(HSV_t *hsv, const uint16_t count)
{
     dma_send(&hsv_filter, hsv, count);
}
